using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public Transform cubeParent;
    public GameObject cubePrefab;
    public ParticleSystem cubeHitEffect;

    public UnityEvent ActionOnCubeHit;

    public float spawnNewCubeDelay = 1f;
    public float slowMotionTimeScale = 0.2f;

    // Start is called before the first frame update
    private void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void OnCubeHit()
    {
        cubeHitEffect.Play();

        Time.timeScale = slowMotionTimeScale;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        StartCoroutine(BackToNormalMotion());
    }

    private IEnumerator BackToNormalMotion()
    {
        yield return new WaitForSecondsRealtime(2f);

        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;

        Instantiate(cubePrefab, cubeParent.position, Quaternion.identity, cubeParent);
    }
}