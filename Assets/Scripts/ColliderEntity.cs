using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderEntity : MonoBehaviour
{
    public string triggerTag = "Player";

    // Start is called before the first frame update
    private void Start()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag != triggerTag) return;

        CollideAction();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != triggerTag) return;

        CollideAction();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag != triggerTag) return;

        CollideAction();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != triggerTag) return;

        CollideAction();
    }

    private void CollideAction()
    {
        GameManager.Instance.OnCubeHit();
        Destroy(gameObject);
    }
}